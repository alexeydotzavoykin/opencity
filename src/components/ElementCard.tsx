import * as React from 'react';
import IElement from '../types/IElement';

import './map.css';

interface IElementProps {
    elementInstance: IElement;
}

export default class ElementCard extends React.Component<IElementProps, any> {
  public render() {
      const {id, position, type} = this.props.elementInstance;
        return (<div> 
            {id}
            {position.lng}
            {position.lat}
            {type}
        </div>);
  }
}