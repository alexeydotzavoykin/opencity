import { Menu } from 'antd';
import * as React from 'react';

export default class LeftMenu extends React.Component {
  public render() {
    return (
   <Menu mode="horizontal">
       <Menu.Item key="Home">
          <a href="">Home</a>
        </Menu.Item>
        <Menu.Item key="Walk">
          <a href="walk">Walk</a>
        </Menu.Item>
        <Menu.Item key="About">
          <a href="about">About</a>
        </Menu.Item>
      </Menu>
    );
  }
}