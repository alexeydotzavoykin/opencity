import * as React from 'react';
import { Glyphicon, Nav, Navbar, NavItem  } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';

export default class Header extends React.Component {
    public render() {
     return (
        <Navbar inverse={true} fixedTop={true} fluid={true} collapseOnSelect={true}>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to={'/home'}>Home</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to={'/route'}>
                    <NavItem>
                        <Glyphicon glyph='map-marker' /> Walk
                    </NavItem>
                </LinkContainer>
                <LinkContainer to={'/about'}>
                    <NavItem>
                        <Glyphicon glyph='exclamation-sign' /> About
                    </NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
        );
     }
}