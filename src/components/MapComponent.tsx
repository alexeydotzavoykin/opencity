import * as React from 'react';
import { Map as LeafletMap, Marker, Popup, TileLayer } from 'react-leaflet';

import '../src/index.css';

export default class MapComponent extends React.Component {
  public render() {
    return (
      <div style={{ width: "90%" }}>
      <LeafletMap
        center={[50, 10]}
        zoom={6}
        maxZoom={10}
        attributionControl={true}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        easeLinearity={0.35}
      >
        <TileLayer
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        
        <Marker position={[50, 10]}>
          <Popup>
            Popup for any custom information.
          </Popup>
        </Marker>
      </LeafletMap>
      </div>
    );
  }
}