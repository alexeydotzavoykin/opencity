import * as React from 'react';
import { Map as LeafletMap, Marker, Popup, TileLayer } from 'react-leaflet';
import IElement from 'src/types/IElement';

import './map.css';

interface IElementMapProps {
  elements: IElement[]
}

export default class ElementMap extends React.Component<IElementMapProps, any> {
  public render() {
    const markersSigns = this.props.elements.map(element => 
    <Marker position={[element.position.lat, element.position.lng]} key={element.id}>
      <Popup>
        {element.type}
      </Popup>
    </Marker>);
    return (
      <LeafletMap
        center={[50, 10]}
        zoom={6}
        maxZoom={10}
        attributionControl={true}
        zoomControl={true}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        easeLinearity={0.35}
      >
        <TileLayer
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        {markersSigns}
      </LeafletMap>
    );
  }
}