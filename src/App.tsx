import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import AboutPage from './pages/AboutPage';
import HomePage from './pages/HomePage';
import WalkPage from './pages/WalkPage';

import './App.css';

export default class App extends React.Component {
  public render() {
    return (
      <BrowserRouter>
      <div style={{ marginTop: 55, marginLeft: 5, marginRight:5 }}>
      <Header />
        <Switch>
          <Route path='/home' component={HomePage}/>
          <Route path='/route' component={WalkPage}/>
          <Route path='/about' component={AboutPage}/>
        </Switch>
      </div>
      </BrowserRouter>
    );
  }
}
