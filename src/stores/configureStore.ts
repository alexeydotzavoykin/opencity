import { connectRouter } from 'connected-react-router';
import { routerMiddleware } from 'react-router-redux';
import { applyMiddleware, combineReducers, compose, createStore, Middleware } from 'redux';
import * as ElementStore from './ElementStore';

export default function configureStore(history: any, initialState: any) {
  const reducers = {
    elementData: ElementStore.reducer,
  };

  const middleware: Middleware[] = [
    // thunk,
    routerMiddleware(history)
  ];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.NODE_ENV === 'development';
  /* tslint:disable:no-string-literal */
  if (isDevelopment && typeof window !== 'undefined' && window['devToolsExtension']) {
    enhancers.push(window['devToolsExtension']);
  /* tslint:enable:no-string-literal */
  }

  const rootReducer = combineReducers({
    ...reducers,
    router: connectRouter(history),
  });

  return createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers)
  );
}
