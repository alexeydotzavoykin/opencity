import { LatLng } from 'leaflet';

import IElement from 'src/types/IElement';

const getElementType = "GET_ELEMENT_TYPE";
const recieveElementType = "RECEIVE_ELEMENT_TYPE";
const deleteElementType = "DELETE_ELEMENT_TYPE";
const removedElementType = "REMOVED_ELEMENT_TYPE";
const updateElementType = "UPDATE_ELEMENT_TYPE";
const updatedElementType = "UPDATED_ELEMENT_TYPE";

const initialState = {
    isLoading: true,
    activeComponent: {
        id: -1,
        type: 1,
        address: "default street",
    },
    BookCommentRates : [],
};

export const actionCreators = {
    requestElement: (elementId: number) => async (dispatch: (data: object) => void, getState: (data: object) => void) => {
        dispatch({ type: getElementType, value: elementId });
        // TODO Replace with APIHelper usage
        const ielement: IElement = {
            id: 1,
            type: 2,
            position: new LatLng(0,0),
        };
        dispatch({ type: recieveElementType, value: ielement });
    },

    removeElement: (elementId: number) => async (dispatch: (data: object) => void, getState: (data: object) => void) => {
        dispatch({ type: deleteElementType, value: elementId });
        // TODO Replace with APIHelper usage
        dispatch({ type: removedElementType});
    },

    updateElement: (element: IElement) => async (dispatch: (data: object) => void, getState: (data: object) => void) => {
        dispatch({ type: updateElementType, value: element });
        // TODO Replace with APIHelper usage
        const ielement: IElement = {
            id: 1,
            type: 3,
            position: new LatLng(0,0),
        };
        dispatch({ type: updatedElementType, value: ielement });
    },

    addElement: () => async (dispatch: (data: object) => void, getState: (data: object) => void) => {
        dispatch({ type: updateElementType, value: "" });
        // TODO Replace with APIHelper usage
        const ielement: IElement = {
            id: 1,
            type: 3,
            position: new LatLng(0,0),
        };
        dispatch({ type: updatedElementType, value: ielement });
    },
}

export const reducer = (state: any, action: any) => {
    state = state || initialState;

    if (action.type === getElementType) {
        return {
            ...state,
            activeComponent: null,
            isLoading: true,
        };
    }

    if (action.type === recieveElementType) {
        return {
            ...state,
            activeComponent: action.value,
            isLoading: false,
        };
    }


    if (action.type === deleteElementType) {
        return {
            ...state,
            isLoading: true,
        };
    }

    if (action.type === removedElementType) {
        return {
            ...state,
            isLoading: false,
        };
    }

    if (action.type === updateElementType) {
        return {
            ...state,
            isLoading: true,
        };
    }

    if (action.type === updatedElementType) {
        return {
            ...state,
            activeComponent: action.value,
            isLoading: false,
        };
    }

    return state;
};