import { Input } from 'antd';
import { LatLng } from 'leaflet';
import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import ElementCard from 'src/components/ElementCard';
import ElementMap from 'src/components/ElementMap';
import { actionCreators } from 'src/stores/ElementStore';
import IElement from 'src/types/IElement';

import "./home.css";

interface IHomeState {
  elementInstance: IElement;
  elements: IElement[];
  lat: string;
  lon: string;
  type: string;
}

class HomePage extends React.Component<any, IHomeState> {
constructor(props: any) {
  super(props);
  this.state = {
    elementInstance: { type: 1, position: new LatLng(50, 10), id: 1},
    elements: [{ type: 1, position: new LatLng(50, 10), id: 1}],
    lat: "",
    lon: "",
    type: ""
  };
  this.handleClick = this.handleClick.bind(this);
  this.handleLatChange = this.handleLatChange.bind(this);
  this.handleLonChange = this.handleLonChange.bind(this);
  this.handleTypeChange = this.handleTypeChange.bind(this);
}

  public render() {
    return (
      <div style={{ minWidth: 300 }}>
          <button onClick={this.handleClick}>Home</button>
          <ElementCard elementInstance={this.state.elementInstance} />
          <ElementMap elements={this.state.elements} />
          <Input onChange={this.handleLatChange} placeholder="lat" value={this.state.lat} className="input" />
          <Input onChange={this.handleLonChange}  placeholder="lon" value={this.state.lon} className="input"/>
          <Input onChange={this.handleTypeChange}  placeholder="type" value={this.state.type} className="input"/>
      </div>
    );
  }
 
 public handleClick() {
  console.log(this.props);
    this.props.addElement();
    this.setState({
      elementInstance: { type: 2, position: new LatLng(+this.state.lon, +this.state.lat), id: 1},
      elements: this.state.elements
        .concat([{ type: +this.state.type, position: new LatLng(+this.state.lon, +this.state.lat), id: 1 }])
    });
  }

  public handleLatChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      lat: e.target.value,
    });
  }

  public handleLonChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      lon: e.target.value,
    });
  }

  public handleTypeChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      type: e.target.value,
    });
  }
}

function mapStateToProps(state: IHomeState) {
  return state;
}
function mapDispatchToProps(dispatch: Dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);