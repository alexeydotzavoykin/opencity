import { LatLng } from 'leaflet';

export default interface IElement {
    id: number,
    type: number, // TODO Replace with enum
    position: LatLng, // TODO Replace with geo data
}